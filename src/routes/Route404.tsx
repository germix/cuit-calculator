import React from 'react';
import { Button } from 'react-bootstrap';
import { RouteComponentProps, withRouter } from 'react-router';

interface Props extends RouteComponentProps
{
}
interface State
{
}

class Route404 extends React.Component<Props,State>
{
    render()
    {
        return <div style={{
            display: 'flex',
            flexDirection: 'column',
        }}>
            <p>¡Ups! No se encontró la página</p>
            <br/>
            <Button onClick={() =>
            {
                this.props.history.replace("/");
            }}>Ir al inicio</Button>
        </div>
    }
}
export default withRouter(Route404);
