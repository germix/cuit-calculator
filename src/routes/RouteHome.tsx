import React from 'react';
import { Card, Container, Form, FormGroup, Row, Col } from 'react-bootstrap';

interface Props
{
}
interface State
{
    type: 'h'|'m'|'e',
    documentNumber: string,
    cuit,
}

class RouteHome extends React.Component<Props,State>
{
    state: State =
    {
        type: 'h',
        documentNumber: '',
        cuit: '---',
    }
    render()
    {
        return (
<>
<Card>
    <Card.Header>
        <Card.Title>Calculador de CUIT</Card.Title>
    </Card.Header>
    <Card.Body>
        <Container>
            <Row>
                <Col>
                    <FormGroup>
                        <Form.Label>
                            Entidad
                        </Form.Label>
                        { this.renderRadio('h','Hombre') }
                        { this.renderRadio('m','Mujer') }
                        { this.renderRadio('e','Empresa') }
                    </FormGroup>
                </Col>
                <Col>
                    <FormGroup>
                        <Form.Label style={{ whiteSpace: 'pre' }}>
                            Número de documento
                        </Form.Label>
                        <Form.Control value={this.state.documentNumber} onChange={(e) =>
                        {
                            this.setState({
                                documentNumber: e.target.value
                            }, () =>
                            {
                                this.calculate();
                            })
                        }}>
                        </Form.Control>
                    </FormGroup>
                </Col>
            </Row>
        </Container>
    </Card.Body>
    <Card.Footer>
        <Row>
            <Col>
                <Form.Label>
                    CUIT:
                </Form.Label>
            </Col>
            <Col>
                <Form.Text>
                    <b> { this.state.cuit } </b>
                </Form.Text>
            </Col>
        </Row>
    </Card.Footer>
</Card>
</>
        )
    }

    renderRadio = (type, label) =>
    {
        return (
            <div onClick={(e) =>
            {
                this.setState({ type }, () =>
                {
                    this.calculate();
                })
            }}>
                <Form.Check checked={this.state.type == type} type='radio' label={label} onChange={(e) =>
                {
                }}/>
            </div>
        )
    }

    calculate = () =>
    {
        // https://www.guiadelcontador.com/detalle.php?a=todo-sobre-el-cuil&t=74&d=292
        // https://es.wikipedia.org/wiki/Clave_%C3%9Anica_de_Identificaci%C3%B3n_Tributaria
        let a = [2,3,4,5,6,7];
        
        try
        {
            let type;
            switch(this.state.type)
            {
                case 'h': type = 20; break;
                case 'm': type = 27; break;
                case 'e': type = 30; break;
                default: throw 'Error: Tipo inválido';
            }
            let tries = 0;
            let dígitoVerificador = null;
            while(tries < 3 && dígitoVerificador == null)
            {
                let nums = ('' + type + '' + this.state.documentNumber).split('').reverse().join('');
                if(nums.length != 10)
                {
                    throw new Error('Invalid document numbers for CUIL/CUIT');
                }

                let sumaP = 0;
                for(let i = 0; i < 10; i++)
                {
                    let n1 = parseInt(nums[i]);
                    let n2 = a[ i % 6 ];

                    sumaP += n1*n2;
                }

                let modulo11 = sumaP % 11;

                let onceMenos = 11 - modulo11;

                dígitoVerificador = null;

                if(onceMenos == 11)
                {
                    dígitoVerificador = 0;
                }
                else if(onceMenos == 10)
                {
                    if(this.state.type == 'e')
                        type = 33;
                    else
                        type = 23;
                }
                else
                {
                    dígitoVerificador = onceMenos;
                }
            }

            if(dígitoVerificador == null)
            {
                throw 'Dígito verificador inválido';
            }
            
            this.setState({
                cuit: type + '-' + this.state.documentNumber + '-' + dígitoVerificador
            });
        }
        catch(error)
        {
            this.setState({
                cuit: '---'
            })
        }
    }
}
export default RouteHome;
