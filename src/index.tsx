import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter, Route, Switch } from "react-router-dom";

import './index.scss';

// Routes
import Route404 from './routes/Route404';
import RouteHome from './routes/RouteHome';

ReactDOM.render(
<BrowserRouter basename={process.env.PUBLIC_URL}>
    <Switch>
        <Route exact path="/" component={RouteHome} />
        <Route component={Route404} />
    </Switch>
</BrowserRouter>
,
document.getElementById("root"));
