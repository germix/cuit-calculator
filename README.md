# CUIT Calculator

## **Calculador de CUIT** en Vercel:

https://cuit-calculator.vercel.app

## **Calculador de CUIT** en GitLab pages:

https://germix.gitlab.io/cuit-calculator

## **Captura**

![](cuit-calculator.png)
